import 'package:flutter/material.dart';

ThemeData light = ThemeData(
  fontFamily: 'Roboto',
  primaryColor: Color(0xFFff0000),
  secondaryHeaderColor: Color(0xFF16202a),
  disabledColor: Color(0xFFBABFC4),
  backgroundColor: Color(0xFFFFFFFF),
  errorColor: Color(0xFFff0000),
  brightness: Brightness.light,
  hintColor: Colors.black,
  cardColor: Colors.white,
  colorScheme: ColorScheme.light(
      primary: Color(0xFFff0000), secondary: Color(0xFFff0000)),
  textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(primary: Color(0xFFff0000))),
);
