import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:divino_menu/controller/auth_controller.dart';
import 'package:divino_menu/controller/cart_controller.dart';
import 'package:divino_menu/controller/location_controller.dart';
import 'package:divino_menu/controller/splash_controller.dart';
import 'package:divino_menu/controller/wishlist_controller.dart';
import 'package:divino_menu/helper/route_helper.dart';
import 'package:divino_menu/util/app_constants.dart';
import 'package:divino_menu/util/dimensions.dart';
import 'package:divino_menu/util/images.dart';
import 'package:divino_menu/view/base/no_internet_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  final String orderID;
  SplashScreen({@required this.orderID});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey();
  StreamSubscription<ConnectivityResult> _onConnectivityChanged;

  @override
  void initState() {
    super.initState();

    bool _firstTime = true;
    _onConnectivityChanged = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (!_firstTime) {
        bool isNotConnected = result != ConnectivityResult.wifi &&
            result != ConnectivityResult.mobile;
        isNotConnected
            ? SizedBox()
            : ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: isNotConnected ? Colors.red : Colors.green,
          duration: Duration(seconds: isNotConnected ? 6000 : 3),
          content: Text(
            isNotConnected ? 'no_connection'.tr : 'connected'.tr,
            textAlign: TextAlign.center,
          ),
        ));
        if (!isNotConnected) {
          _route();
        }
      }
      _firstTime = false;
    });

    Get.find<SplashController>().initSharedData();
    Get.find<CartController>().getCartData();
    _route();
  }

  @override
  void dispose() {
    super.dispose();

    _onConnectivityChanged.cancel();
  }

  void _route() {
    Get.find<SplashController>().getConfigData().then((isSuccess) {
      if (isSuccess) {
        Timer(Duration(seconds: 1), () async {
          int _minimumVersion = 0;
          if (GetPlatform.isAndroid) {
            _minimumVersion = Get.find<SplashController>()
                .configModel
                .appMinimumVersionAndroid;
          } else if (GetPlatform.isIOS) {
            _minimumVersion =
                Get.find<SplashController>().configModel.appMinimumVersionIos;
          }
          if (AppConstants.APP_VERSION < _minimumVersion ||
              Get.find<SplashController>().configModel.maintenanceMode) {
            Get.offNamed(RouteHelper.getUpdateRoute(
                AppConstants.APP_VERSION < _minimumVersion));
          } else {
            if (widget.orderID != null) {
              Get.offNamed(
                  RouteHelper.getOrderDetailsRoute(int.parse(widget.orderID)));
            } else {
              if (Get.find<AuthController>().isLoggedIn()) {
                Get.find<AuthController>().updateToken();
                await Get.find<WishListController>().getWishList();
                if (Get.find<LocationController>().getUserAddress() != null) {
                  Get.offNamed(RouteHelper.getInitialRoute());
                } else {
                  Get.offNamed(RouteHelper.getAccessLocationRoute('splash'));
                }
              } else {
                if (Get.find<SplashController>().showIntro()) {
                  if (AppConstants.languages.length > 1) {
                    Get.offNamed(RouteHelper.getOnBoardingRoute());
                  } else {
                    Get.offNamed(RouteHelper.getOnBoardingRoute());
                  }
                } else {
                  Get.offNamed(RouteHelper.getSignInRoute(RouteHelper.splash));
                }
              }
            }
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      body: GetBuilder<SplashController>(builder: (splashController) {
        return Center(
          child: splashController.hasConnection
              ? Container(
                  decoration: BoxDecoration(
                    //color: Theme.of(context).scaffoldBackgroundColor,
                    color: Color(0xFFed1e24),
                  ),
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          //height: 150,
                          width: 250,
                          child: Image.asset(
                            Images.logo,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : NoInternetScreen(child: SplashScreen(orderID: widget.orderID)),
        );
      }),
    );
  }
}
